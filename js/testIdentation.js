function calculateTotal(price, quantity){
return price*quantity;
}

const items = [
{ name: 'Item 1', price: 20, quantity: 2 },
{ name: 'Item 2', price: 15, quantity: 3 },
{ name: 'Item 3', price: 10, quantity: 5 }
];

for(let i=0;i<items.length;i++){
const item = items[i];
if(item.quantity > 2){
console.log(`Total for ${item.name}: ${calculateTotal(item.price, item.quantity)}`);
} else{
console.log(`Quantity is less than 2 for ${item.name}`);
}
}

function calcularPrecioTotal(producto, cantidad) {
if(producto === 'ProductoA'){
return 10 * cantidad;
} else if (producto === 'ProductoB'){
return 15 * cantidad;
} else {
return 5 * cantidad;
}
}

const carrito = [
{producto: 'ProductoA', cantidad: 2},
{producto: 'ProductoB', cantidad: 3},
{producto: 'ProductoC', cantidad: 5}
];

for(let i=0;i<carrito.length;i++){
const item = carrito[i];
if(item.cantidad > 2){
console.log(`Precio total para ${item.producto}: ${calcularPrecioTotal(item.producto, item.cantidad)}`);
} else{
console.log(`La cantidad es menor que 2 para ${item.producto}`);
}
}



console.log('Fin del programa');
  
    